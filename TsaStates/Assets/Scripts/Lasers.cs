﻿using UnityEngine;
using System.Collections;

public class Lasers : MonoBehaviour {
	public Rigidbody laserPrefab;
	public Transform ship;
	public bool canShoot = true;
	public float Delay;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if ( canShoot && Input.GetKey("space")) {
			StartCoroutine ( Fire() );
		}
	}

	IEnumerator Fire () {
	 	Rigidbody laserInstance;
		laserInstance = Instantiate (laserPrefab, ship.position, ship.rotation) as Rigidbody;
		laserInstance.AddForce (1 * 750, 0, 0);
		canShoot = false;
		yield return new WaitForSeconds(Delay);
		canShoot = true;
	}
}