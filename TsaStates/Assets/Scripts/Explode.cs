﻿using UnityEngine;
using System.Collections;

public class Explode : MonoBehaviour {
	public float Delay;
	void OnCollisionEnter (Collision col) {
		if (col.gameObject.name == "laser(Clone)") {
			Destroy (gameObject);
			Destroy (col.gameObject);
			Debug.Log ("Hit");
		}
		if (col.gameObject.name == "Space") {
			Application.LoadLevel (Application.loadedLevelName);
		}
	}
}
