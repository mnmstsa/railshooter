﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Movement : MonoBehaviour {
	private Rigidbody rb;
	public int forward;
	public int controlSpeed;
	private Vector3 movement;
	private Quaternion direction;
	public int rotSpeed;
	Animator animator;
	public int score;
	public Text scoreText;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

	}

	void Update () {
		float Vertical = Input.GetAxis ("Vertical");
		float Horizontal = Input.GetAxis ("Horizontal");
		movement = new Vector3 (forward, Vertical * controlSpeed, Horizontal * -controlSpeed);
		rb.MovePosition (transform.localPosition + movement * Time.deltaTime);
		bool left = Input.GetKeyDown (KeyCode.LeftArrow);
		bool right = Input.GetKeyDown (KeyCode.RightArrow);
		bool up = Input.GetKeyDown (KeyCode.UpArrow);
		bool down = Input.GetKeyDown (KeyCode.DownArrow);
		animator.SetBool("Left", left);
		animator.SetBool ("Right", right);
		animator.SetBool ("Up", up);
		animator.SetBool ("Down", down);

		if (!Input.anyKey) {
			animator.Play ("Player_Default");
		}
	}

	void Animating ()
	{
		if (Input.GetKeyDown(KeyCode.LeftArrow)) {
			transform.RotateAround(transform.position, transform.up, Time.deltaTime * 180f);
		}
	}

	void OnTriggerEnter(Collider col) {
		if (col.gameObject.CompareTag ("Coin")) {
			col.gameObject.SetActive (false);
			score = score + 1;
			Debug.Log (score);
			scoreText.text = "Score: " + score.ToString ();
		}
	}
		
}
